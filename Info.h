#ifndef LIST
#define LIST
#include <iostream>
#include <string>
#include <list>
using namespace std;
class Info 
{
public:
	Info() {};
	void set_id(int id);
	void set_name(string name);
	int get_id();
	string get_name();
private:
	int std_id;
	string std_name;
};
#endif //LIST