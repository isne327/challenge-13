#include"Info.h"
int hash_id(int id);
int hash_name(string name);

int main()
{
	Info info,tmp;
	list<Info> data_id[50];
	list<Info> data_name[50];
	int id,choice;
	string name;
	while (true) //Entering Student ID and Names phase
	{
		cout << "Input the student ID (enter 0 to stop input)" << endl;
		cin >> id;
		cin.ignore(); //Ignore "enter" once
		if (id == 0)
		{
			break;
		}
		info.set_id(id);
		cout << "Input the student name " << endl;
		getline(cin, name);
		info.set_name(name);
		data_id[hash_id(id)].push_back(info);
		data_name[hash_name(name)].push_back(info);
	}
	while (true) //Choosing search method phase
	{
		cout << "Enter choices :" << endl << ":: 1 to search by ID" << endl << ":: 2 to search by name" << endl << ":: 0 to exit" << endl;
		cin >> choice;
		cin.ignore();
		if (choice == 0)
		{
			break;
		}

		if (choice == 1) //Search by ID
		{
			while (true)
			{
				cout << "Enter the student ID (enter 0 to exit)" << endl;
				cin >> id;
				if (id == 0)
				{
					break;
				}
				list<Info>::iterator it = data_id[hash_id(id)].begin();
				Info temp;
				while (true)
				{
					temp = *it;
					if (temp.get_id() == id) 
					{
						cout << "******************************************" << endl;
						cout << "Name of Student ID " << id << " is " << temp.get_name() << endl;
						cout << "******************************************" << endl;
						break;
					}
					else
					{
						it++;
					}
				}
			}

		}
		else if (choice == 2) //Search by name
		{
			while (true)
			{
				cout << "Enter student name (type 'exit' to exit)" << endl;
				getline(cin, name);
				if (name == "exit")
				{
					break;
				}
				list<Info>::iterator ptr = data_name[hash_name(name)].begin();
				Info temp2;
				while (true)
				{
					temp2 = *ptr;
					if (temp2.get_name() == name) 
					{
						cout << "******************************************" << endl;
						cout << "ID of Student name " << name << " is " << temp2.get_id() << endl;
						cout << "******************************************" << endl;
						break;
					}
					else
					{
						ptr++;
					}
				}
			}
			
		}
	}
	

}

int hash_id(int id) //return the last two digits of the ID
{
	return id % 100;
}

int hash_name(string name) //Change from string to an interger of 0-49
{
		int sum = 0;
		for (int i = 0; i < name.length(); i++) {
			char x = name.at(i);
			sum = sum + int(x);
		}
		return sum % 50;
}